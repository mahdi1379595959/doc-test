#!/usr/bin/env bash
set -euo pipefail

REPO=$1
PROJECT_NAME=$2
shift 2
DIRS=( "$@" )

set +e
git clone -n "${REPO}" "src/${PROJECT_NAME}" --depth 1 > /dev/null
set -e

cd "src/${PROJECT_NAME}"

git checkout HEAD "${DIRS[@]}"