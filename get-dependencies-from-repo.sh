#!/usr/bin/env bash

while IFS= read -r line
do
  IFS=" " read -r -a dependency <<< "$line"
  ./get-subdirectory-from-repo.sh "${dependency[@]}"
done < dependencies